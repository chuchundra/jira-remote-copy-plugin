package com.atlassian.cpji.config;

/**
 * Категории из регламента (они же тип запроса в JSD)
 */
public enum IssueCategories {

    REQUEST_FOR_SERVICE("Запрос на обслуживание"),
    REQUEST_FOR_REWORK("Запрос на доработку"),
    REQUEST_FOR_CONSULTATION("Запрос на консультацию"),
    REQUEST_FOR_MODIFICATION("Запрос на изменение"),
    INCIDENT("Инцидент");

    private String caption;

    IssueCategories(String caption) {
        this.caption = caption;
    }

    public String getCaption() {
        return caption;
    }

}
